import matplotlib.pyplot as plt
import pandas as pd
import os
import re
import itertools
import numpy as np


directory = '/home/pierrealex/Cloud3000/Work/Advanced Energy/Code/ALISA/qchem_out/'

def set_molecules_data():
    data = {}
    for filename in os.listdir(directory):
        M = []
        F = []
        I = []

        for line in open(directory + filename, 'r').readlines():
            if 'Mode:' in line:
                M.append(re.findall(r"[-+]?\d*\.\d+|\d+", line))  #)
            elif 'Frequency:' in line:                            #)
                F.append(re.findall(r"[-+]?\d*\.\d+|\d+", line))  # }find floats in line using regex
            elif 'IR Intens:' in line:                            #)
                I.append(re.findall(r"[-+]?\d*\.\d+|\d+", line))  #)

        modes = list(itertools.chain.from_iterable(M))
        freqs = list(itertools.chain.from_iterable(F))
        IR_int = list(itertools.chain.from_iterable(I))

        data[str(filename).strip('.out')] = {}
        for i in range(len(modes)):
            data[str(filename).strip('.out')][i] = [round(float(freqs[i]), 0), float(IR_int[i])]

    return data

mols = set_molecules_data()
X = np.arange(0, 4000, 1)

wn = []
for key in mols.get('C6H6'):
    wn.append(mols.get('C6H6').get(key)[0])

Y=[]
j = 0
for i in range(len(X)):
    if i in wn:
        Y.append(mols.get('C6H6')[j][1])
        j += 1
    else:
        Y.append(0)

plt.bar(X, Y)
plt.show()