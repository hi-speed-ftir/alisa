class Parameters:

    def __init__(self):
        self.models = ['Logistic Regression', 'Perceptron']

        self.batch_parameters = {'Batch size': 100,
                                 'Std min': 70,
                                 'Std max': 130,
                                 'Noise level': 0.05,
                                 'Drift +/- (%)': 0.01}
        self.batch_parameters_lines = {}

        self.single_parameters = {'STD': 100,
                                  'Drift (%)': 0.01,
                                  'Noise level': 0.025,
                                  'Humidity factor': 50}
        self.single_parameters_lines = {}

        self.classes_lines = {}

