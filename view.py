from PyQt5.QtWidgets import *
import PyQt5.QtWidgets
from PyQt5 import QtGui, QtCore
from PyQt5.QtGui import QPixmap, QFont
from matplotlib.backends.backend_qt5agg import FigureCanvas
from matplotlib.figure import Figure
from matplotlib.backends.backend_qt5agg import (NavigationToolbar2QT as NavigationToolbar)
import pandas as pd
from functools import partial
import controller as c
import parameters as params
import molecules as mols

class Window(QDialog):

    def __init__(self):
        super().__init__()

        ##Objects##
        self.parameters = params.Parameters()
        self.molecules = mols.Molset()
        self.controller = c.Controller(self, self.parameters, self.molecules)

        ##Window Parameters##
        self.width = 1440
        self.height = 810
        self.img_size = 250

        self.titles_Font = QFont('Helvetica', 16, QFont.Bold)
        self.sub_titles_Font = QFont('Helvetica', 10, QFont.Bold)

        ##Initialize window##
        self.setWindowTitle("ALISA")
        self.setGeometry(0, 0, self.width, self.height)
        self.make_window()
        self.showMaximized()
        self.show()

        self.controller.new_nn()

    def make_window(self):
        mainBox = QHBoxLayout()
        self.setLayout(mainBox)

        vbox_1 = QVBoxLayout()
        vbox_1.addWidget(self.main_buttons_box(), 5)
        vbox_1.addWidget(self.batch_operations_box(), 25)
        vbox_1.addWidget(self.single_operations_box(), 25)
        vbox_1.addWidget(self.log_box(), 55)

        vbox_2 = QVBoxLayout()
        vbox_2.addWidget(self.main_graph_box(), 70)
        vbox_2.addWidget(self.image_box(), 30)

        vbox_3 = QVBoxLayout()
        vbox_3.addWidget(self.coef_graph_box(), 50)
        vbox_3.addWidget(self.test_results_box(), 50)

        mainBox.addLayout(vbox_1, 25)
        mainBox.addLayout(vbox_2, 38)
        mainBox.addLayout(vbox_3, 37)

    def main_buttons_box(self):
        group = QGroupBox()
        hBox = QHBoxLayout()

        label = QLabel()
        label.setMaximumHeight(50)
        logo = QPixmap('images/AELogo.png')
        scaled_logo = logo.scaledToHeight(label.height())
        label.setPixmap(scaled_logo)

        vBox = QVBoxLayout()
        self.model_slct = QComboBox()
        for item in self.parameters.models:
            self.model_slct.addItem(item)
        vBox.addWidget(self.model_slct)

        btn_hbox = QHBoxLayout()
        self.new_btn = QPushButton('New Neural Network')
        self.new_btn.clicked.connect(self.controller.new_nn)
        self.load_btn = QPushButton('Load Neural Network')

        btn_hbox.addWidget(self.new_btn)
        btn_hbox.addWidget(self.load_btn)

        vBox.addLayout(btn_hbox)

        hBox.addWidget(label, 20)
        hBox.addLayout(vBox, 80)

        group.setLayout(hBox)

        return group

    def batch_operations_box(self):
        group = QGroupBox()
        vBox = QVBoxLayout()

        title = QLabel('Batch Operations')
        title.setFont(self.titles_Font)
        title.setAlignment(QtCore.Qt.AlignCenter)
        vBox.addWidget(title)

        for key in self.parameters.batch_parameters:
            hBox = QHBoxLayout()
            self.line = QLineEdit(key)
            self.line.setText(str(self.parameters.batch_parameters[key]))
            self.parameters.batch_parameters_lines[key] = self.line
            label = QLabel()
            label.setText(key + ": ")
            hBox.addWidget(label)
            hBox.addWidget(self.line)
            vBox.addLayout(hBox)

        self.balance_checkbox = QCheckBox('Generate balanced batch')

        vBox.addWidget(self.balance_checkbox)

        actions_vbox = QVBoxLayout()
        buttons_box = QHBoxLayout()

        train_button = QPushButton("Train!")
        train_button.clicked.connect(self.train)

        batch_test_button = QPushButton("Batch Test!")
        part = partial(self.test, 'batch')
        batch_test_button.clicked.connect(part)

        self.progress = QProgressBar()

        buttons_box.addWidget(train_button)
        buttons_box.addWidget(batch_test_button)
        actions_vbox.addLayout(buttons_box)
        actions_vbox.addWidget(self.progress)

        vBox.addLayout(actions_vbox)
        group.setLayout(vBox)

        return group

    def single_operations_box(self):
        group = QGroupBox()
        vBox = QVBoxLayout()

        center_title = QLabel('Single Plotting & Testing')
        center_title.setFont(self.titles_Font)
        center_title.setAlignment(QtCore.Qt.AlignCenter)
        vBox.addWidget(center_title)

        self.mol_slct = QComboBox()
        for key in self.molecules.molecules_data:
            self.mol_slct.addItem(key)
        vBox.addWidget(self.mol_slct)

        for key in self.parameters.single_parameters:
            hBox = QHBoxLayout()
            self.line = QLineEdit(key)
            self.line.setText(str(self.parameters.single_parameters[key]))
            self.parameters.single_parameters_lines[key] = self.line
            label = QLabel()
            label.setText(key + ": ")
            hBox.addWidget(label)
            hBox.addWidget(self.line)
            vBox.addLayout(hBox)

        plot_button = QPushButton('Quick Plot')
        plot_button.clicked.connect(self.quick_plot)

        test_button = QPushButton('Test!')
        part = partial(self.test, 'single')
        test_button.clicked.connect(part)

        vBox.addWidget(plot_button)
        vBox.addWidget(test_button)

        group.setLayout(vBox)

        return group

    def log_box(self):
        group = QGroupBox()
        vBox = QVBoxLayout()

        title = QLabel('Application log')
        title.setFont(self.titles_Font)
        title.setAlignment(QtCore.Qt.AlignCenter)
        vBox.addWidget(title)

        self.log = QTextEdit()
        self.log.setReadOnly(True)
        self.log.setText("Hello, I'm ALISA, an Automatic Logistic Infrared Spectrum Analyzer \n")

        vBox.addWidget(self.log)

        group.setLayout(vBox)

        return group

    def main_graph_box(self):
        group = QGroupBox()
        vBox = QVBoxLayout()

        title = QLabel('IR Spectrum Graph')
        title.setFont(self.titles_Font)
        title.setAlignment(QtCore.Qt.AlignCenter)
        vBox.addWidget(title)

        self.main_graph = MplWidget()
        self.main_graph_TB = NavigationToolbar(self.main_graph.canvas, self)

        vBox.addWidget(self.main_graph)
        vBox.addWidget(self.main_graph_TB)

        group.setLayout(vBox)

        return group

    def image_box(self):
        group = QGroupBox()
        vBox = QVBoxLayout()

        self.img_title = QLabel('ALISA')
        self.img_title.setFont(self.titles_Font)
        self.img_title.setAlignment(QtCore.Qt.AlignCenter)
        vBox.addWidget(self.img_title)

        self.img_label = QLabel()
        self.img_label.setAlignment(QtCore.Qt.AlignCenter)
        self.img_label.setMaximumHeight(403)
        self.img = QPixmap('images/ALISA.png')
        scaled_img = self.img.scaledToHeight(self.img_label.height())
        self.img_label.setPixmap(scaled_img)
        vBox.addWidget(self.img_label)

        group.setLayout(vBox)

        return group

    def coef_graph_box(self):
        group = QGroupBox()
        vBox = QVBoxLayout()

        title = QLabel('Neural Network Coefficients')
        title.setFont(self.titles_Font)
        title.setAlignment(QtCore.Qt.AlignCenter)
        vBox.addWidget(title)

        self.coef_graph = MplWidget()

        vBox.addWidget(self.coef_graph)

        self.open_btn = QPushButton('Open graph')
        self.open_btn.clicked.connect(self.open_graph)
        vBox.addWidget(self.open_btn)

        group.setLayout(vBox)

        return group

    def test_results_box(self):
        group = QGroupBox()
        self.results_vBox = QVBoxLayout()

        title = QLabel("Results")
        title.setFont(self.titles_Font)
        title.setAlignment(QtCore.Qt.AlignCenter)
        self.results_vBox.addWidget(title)

        self.results_graph = MplWidget()
        self.results_graph_TB = NavigationToolbar(self.results_graph.canvas, self)

        self.results_vBox.addWidget(self.results_graph)
        self.results_vBox.addWidget((self.results_graph_TB))

        group.setLayout(self.results_vBox)

        return group

    def train(self):
        self.controller.train()

    def test(self, test_type):
        self.controller.test(test_type)

    def open_graph(self):
        self.controller.open_graph()

    def quick_plot(self):
        self.controller.quick_plot()

    def plot_main(self, spectrum):
        self.main_graph.canvas.axes.clear()
        self.main_graph.canvas.axes.set_xlabel('WN (cm-1)')
        self.main_graph.canvas.axes.set_ylabel('Absorbance')
        self.main_graph.canvas.axes.set_title(spectrum.mol)
        spectrum.spectrum_data['plot'].plot(ax=self.main_graph.canvas.axes, color="orange")

        self.main_graph.canvas.draw()
        self.set_image(spectrum.mol)

    def plot_nn_coefs(self, data):
        self.coef_graph.canvas.axes.clear()
        self.coef_graph.canvas.axes.set_xlabel("WN (cm-1)")
        self.coef_graph.canvas.axes.set_ylabel("Coef. value")
        d = pd.DataFrame(data).T
        for i in range(len(data)):
            d[i].plot(ax=self.coef_graph.canvas.axes)

        self.coef_graph.canvas.draw()

    def plot_results(self, type, data):
        self.clear_results_box()

        self.results_graph.canvas.axes.clear()

        if type == 'Predictions':
            self.results_graph.canvas.axes.clear()

            values = []
            labels = []

            for item in data:
                if item != "Predicted class":
                    labels.append(item)
                    values.append(data.get(item))

            self.results_graph.canvas.axes.barh(labels, values, color='orange')
            self.results_graph.canvas.axes.set_title("Predictions")
            self.results_graph.canvas.draw()

        elif type == 'Matrix':
            self.results_graph.canvas.axes.clear()

            data.plot(ax=self.results_graph.canvas.axes)
            self.results_graph.canvas.axes.set_title("Confusion Matrix")
            self.results_graph.canvas.draw()

        elif type == 'Training set':
            self.results_graph.canvas.axes.clear()

            values = list(data.values())
            labels = list(data.keys())

            self.results_graph.canvas.axes.bar(labels, values, color='orange')
            self.results_graph.canvas.axes.set_title("Training set")
            self.results_graph.canvas.draw()

    def set_log_text(self, text):
        self.log.append('> ' + text + '\n')

    def set_progress(self, value, text):
        self.progress.setFormat(text)
        self.progress.setValue(value)

    def set_classes_proba(self, data):
        for key in data:
            if key != 'Predicted class':
                self.parameters.classes_lines.get(key).setText(key + ': ' + str(round(data.get(key) * 100, 3)) + ' %')
                if str(data.get('Predicted class')[0]) == key:
                    self.parameters.classes_lines.get(key).setFont(self.sub_titles_Font)
                else:
                    self.parameters.classes_lines.get(key).setFont(QFont())

    def set_image(self, mol):
        path = 'images/' + mol + '.png'
        img = QPixmap(path)
        scaled_img = img.scaledToHeight(self.img_label.height())
        self.img_label.setPixmap(scaled_img)
        self.img_title.setText(mol)

    def clear_results_box(self):
        for i in reversed(range(self.results_vBox.count())):
            self.results_vBox.itemAt(i).widget().setParent(None)

        self.results_graph = MplWidget()
        self.results_graph_TB = NavigationToolbar(self.results_graph.canvas, self)

        self.results_vBox.addWidget(self.results_graph)
        self.results_vBox.addWidget((self.results_graph_TB))

    def reset_nn(self):
        self.controller.new_nn()

    def save_nn(self):
        self.controller.save_nn()


class MplWidget(PyQt5.QtWidgets.QWidget):

    def __init__(self, parent=None):
        PyQt5.QtWidgets.QWidget.__init__(self, parent)

        self.canvas = FigureCanvas(Figure())

        vertical_layout = PyQt5.QtWidgets.QVBoxLayout()
        vertical_layout.addWidget(self.canvas)

        self.canvas.axes = self.canvas.figure.add_subplot(111)
        self.setLayout(vertical_layout)


class Graph_window(QDialog):

    def __init__(self, nn):
        super().__init__()

        self.setWindowTitle('Graph Window')
        self.setGeometry(0, 0, 800, 600)
        self.titles_Font = QFont('Helvetica', 16, QFont.Bold)
        self.sub_titles_Font = QFont('Helvetica', 10, QFont.Bold)

        self.nn = nn
        self.data_lines = {}

        self.InitUI()
        # self.showMaximized()
        self.show()
        self.plot()

    def InitUI(self):
        mainVBox = QHBoxLayout()
        self.setLayout(mainVBox)

        mainVBox.addWidget(self.graph_box(), 85)
        mainVBox.addWidget(self.graph_selecter(), 15)

    def graph_box(self):
        group = QGroupBox()
        vBox = QVBoxLayout()

        title = QLabel("Neural Network Coefficients")
        title.setFont(self.titles_Font)
        title.setAlignment(QtCore.Qt.AlignCenter)
        vBox.addWidget(title, 5)

        self.graph = MplWidget()
        self.graph_TB = NavigationToolbar(self.graph.canvas, self)

        vBox.addWidget(self.graph, 85)
        vBox.addWidget(self.graph_TB, 10)

        group.setLayout(vBox)

        return group

    def graph_selecter(self):
        group = QGroupBox()
        vBox = QVBoxLayout()

        title = QLabel("Classes to plot")
        title.setFont(self.titles_Font)
        title.setAlignment(QtCore.Qt.AlignCenter)
        vBox.addWidget(title, 5)

        vBox_2 = QVBoxLayout()
        for item in self.nn.NN.classes_:
            hbox = QHBoxLayout()
            check_box = QCheckBox()
            check_box.setChecked(True)
            check_box.toggled.connect(self.plot)
            self.data_lines[item] = check_box
            hbox.addWidget(check_box)

            label = QLabel(str(item))
            label.setFont(self.sub_titles_Font)
            hbox.addWidget(label)

            vBox_2.addLayout(hbox)

        vBox.addLayout(vBox_2, 95)

        group.setLayout(vBox)

        return group


    def plot(self):
        self.graph.canvas.axes.clear()

        self.graph.canvas.axes.set_xlabel("WN (cm-1)")
        self.graph.canvas.axes.set_ylabel("Coef. value")
        d = pd.DataFrame(self.nn.NN.coef_).T

        for item in self.nn.NN.classes_:
            if self.data_lines.get(item).isChecked():
                d[self.nn.NN.classes_.tolist().index(item)].plot(ax=self.graph.canvas.axes, label=item)

        self.graph.canvas.axes.legend()
        self.graph.canvas.draw()

