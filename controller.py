from PyQt5.QtWidgets import *
import sys
import random
import numpy as np
import view as v
import model as m


App = QApplication(sys.argv)

class Controller:

    def __init__(self, window, parameters, molset):
        self.win = window
        self.parameters = parameters
        self.molset = molset

        self.reset_mol_batch()

    def reset_mol_batch(self):
        self.molecules_batch = {}
        for key in self.molset.molecules_data:
            self.molecules_batch[key] = 0

    def new_nn(self):
        self.win.set_image('ALISA')
        self.win.set_log_text('New {} Neural Network ready'.format(self.win.model_slct.currentText()))

        self.nn = m.Neural_Network(self.win.model_slct.currentText())

    def quick_plot(self):
        mol = self.win.mol_slct.currentText()
        mol_modes = self.molset.molecules_data.get(mol)
        sp = m.Spectrum_parameters(self.parameters, 'single')
        spectrum = m.Spectrum(mol, mol_modes, sp.spect_parameters)
        self.win.plot_main(spectrum)

    def open_graph(self):
        self.graph_window = v.Graph_window(self.nn)

    def train(self):
        self.win.set_log_text('Training start!')

        train_batch = m.Data_batch(self.parameters, self.win, self.molset)
        score = self.nn.train(data=train_batch.batch)

        self.nn.training_rounds = self.nn.training_rounds + eval(self.parameters.batch_parameters_lines.get('Batch size').text())
        self.win.plot_nn_coefs(self.nn.NN.coef_)
        self.win.set_progress(100, "Training done")
        self.win.set_log_text("Training set: " + str(train_batch.molecules_batch))
        self.win.plot_results('Training set', train_batch.molecules_batch)
        self.win.set_log_text("Training done. Score: " + str(score))

    def test(self, test_type):
        if test_type == 'batch':
            self.win.set_log_text('Batch testing...')

            test_batch = m.Data_batch(self.parameters, self.win, self.molset)
            P = self.nn.test("Batch", test_batch.batch)

            guessed = 0
            i = 0
            for item in P:
                if item == test_batch.batch['y'][i]:
                    guessed += 1
                i += 1

            accuracy = (guessed * 100) / eval(self.parameters.batch_parameters_lines.get('Batch size').text())

            self.win.plot_results('Matrix', self.nn.CMD)

            self.win.set_progress(100, 'Batch testing done')
            self.win.set_log_text("Testing set: " + str(test_batch.molecules_batch))
            self.win.set_log_text("Accuracy: " + str(round(accuracy, 4)) + '%, ' + "F1 score: " + str(round(self.nn.F1, 2)))

        elif test_type == 'single':
            mol = self.win.mol_slct.currentText()
            mol_modes = self.molset.molecules_data.get(mol)
            sp = m.Spectrum_parameters(self.parameters, 'single')
            spectrum = m.Spectrum(mol, mol_modes, sp.spect_parameters)
            self.win.plot_main(spectrum)

            P = self.nn.test("Single", spectrum.spectrum_data['input'])

            self.win.plot_results('Predictions', P)
            self.win.set_log_text("Model predicts molecule: " + str(P.get('Predicted class')[0]) + ' at ' + str(P.get(P.get('Predicted class')[0])*100) + ' %')

    def save_nn(self):
        pass

    def load_nn(self):
        pass

if __name__ == '__main__':
    window = v.Window()
    sys.exit(App.exec())

