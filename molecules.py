import os
import re
import itertools

class Molset:

    def __init__(self):
        self.dir = 'qchem_out/'
        self.molecules_data = self.set_molecules_data()
        self.molecules_list = list(self.molecules_data.keys())

    def set_molecules_data(self):
        data = {}
        for filename in os.listdir(self.dir):
            M = []
            F = []
            I = []

            for line in open(self.dir + filename, 'r').readlines():
                if 'Mode:' in line:
                    M.append(re.findall(r"[-+]?\d*\.\d+|\d+", line))  #)
                elif 'Frequency:' in line:                            #)
                    F.append(re.findall(r"[-+]?\d*\.\d+|\d+", line))  # }find floats in line using regex
                elif 'IR Intens:' in line:                            #)
                    I.append(re.findall(r"[-+]?\d*\.\d+|\d+", line))  #)

            modes = list(itertools.chain.from_iterable(M))
            freqs = list(itertools.chain.from_iterable(F))
            IR_int = list(itertools.chain.from_iterable(I))

            data[str(filename).strip('.out')] = {}
            for i in range(len(modes)):
                data[str(filename).strip('.out')][i] = [round(float(freqs[i]), 0), float(IR_int[i])]

        return data
