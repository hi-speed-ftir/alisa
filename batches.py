import numpy as np
import random
import model as m


class Batch:

    def __init__(self, window, parameters, molset):
        self.win = window
        self.parameters = parameters
        self.molset = molset

        self.data_batch = self.produce_batch()


    def produce_batch(self):
        X = []
        y = []

        for i in range(self.parameters.batch_parameters.get('Batch size')):
            mol = self.molset.molecules_list[random.randint(0, len(self.molset.molecules_list) - 1)]
            mol_modes = self.molset.molecules_data.get(mol)
            self.molecules_trained[mol] += 1
            spect_parameters = {'STD': random.randint(
                (self.parameters.batch_parameters.get('Std min'), self.parameters.batch_parameters.get('Std max'))),
                                'Max noise': eval(self.parameters.batch_parameters.get('Max noise')),
                                'Drift': random.randint(-eval(self.parameters.batch_parameters.get('Max General drift')),
                                                        eval(self.parameters.batch_parameters.get('Max General drift')))}
            spectrum = m.Spectrum(mol_modes, spect_parameters)
            self.win.plot_batch_spectrum(spectrum, mol)
            X.append(spectrum.spectrum_data['neg noisy spectrum'])
            y.append(mol)

            self.win.set_progress((i * 100) / self.parameters.batch_parameters.get('Batch size'),
                                  'Generating training batch')

        X = np.array(X)
        y = np.array(y)

        batch = {'X': X, 'y': y}

        return batch