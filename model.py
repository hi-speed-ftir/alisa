from sklearn.linear_model import LogisticRegression, Perceptron
from sklearn.metrics import confusion_matrix, ConfusionMatrixDisplay, f1_score
import random
import numpy as np
import pandas as pd

class Neural_Network:

    def __init__(self, type):
        self.type = type
        self.training_rounds = 0

        self.model_select()

    def model_select(self):
        if self.type == 'Logistic Regression':
            self.NN = LogisticRegression(warm_start=True,
                                         random_state=0,
                                         max_iter=150,
                                         solver='newton-cg',
                                         class_weight='balanced')

        elif self.type == 'Perceptron':
            self.NN = Perceptron(warm_start=True,
                                 random_state=0,
                                 max_iter=100,
                                 class_weight='balanced')

    def train(self, data):
        self.NN.fit(X=data['X'], y=data['y'])
        return self.NN.score(data['X'], data['y'])

    def test(self, type, data):
        if type == 'Single':
            P = {}
            i = 0
            for item in self.NN.classes_:
                P[item] = round(self.NN.predict_proba(data.values.reshape(1, -1))[0].tolist()[i], 4)
                i += 1
            P['Predicted class'] = self.NN.predict(data.values.reshape(1, -1))

            return P

        elif type == 'Batch':
            P = self.NN.predict(X=data['X'])

            self.F1 = f1_score(data['y'], P, average='micro')
            self.cm = confusion_matrix(data['y'], P, labels=self.NN.classes_)
            self.CMD = ConfusionMatrixDisplay(confusion_matrix=self.cm, display_labels=self.NN.classes_)

            return P

class Spectrum:

    def __init__(self, mol, mol_modes, parameters):
        self.mol = mol
        self.modes_data = mol_modes
        self.spectrum_parameters = parameters

        ##Calculate spectrum graph##
        self.drifted_modes = self.apply_drift()
        self.spectrum_data = pd.DataFrame()
        self.generate_spectrum_line()
        self.add_noise()
        self.spectrum_data['input'] = self.spectrum_data['noisy spectrum'] / max(self.spectrum_data['noisy spectrum'])
        self.spectrum_data['plot'] = self.spectrum_data['neg noisy spectrum']

    def apply_drift(self):
        data = {}
        for key in self.modes_data:
            data[key] = [self.modes_data[key][0] + (self.spectrum_parameters.get('Drift') * 3600), self.modes_data[key][1]] #Applies a percentage of the whole scale (3600) as drift

        return data

    def generate_spectrum_line(self):
        self.spectrum_data['X'] = np.arange(400, 4000, 1)

        def gauss(list, std):
            U = np.arange(400, 4000, 1)
            V = []

            for u in U:
                v = list[1] / (std * np.sqrt(2 * np.pi)) * np.exp(-(u - list[0]) ** 2 / (2 * std ** 2))
                V.append(round(v, 6))

            return V

        for key in self.drifted_modes:
            self.spectrum_data[key] = gauss(self.drifted_modes.get(key), self.spectrum_parameters.get('STD'))

        self.spectrum_data['spectrum'] = self.spectrum_data.drop('X', axis=1).sum(axis=1)
        self.spectrum_data['neg spectrum'] = - self.spectrum_data['spectrum']

    def add_noise(self):
        noisy_spectrum = []
        max_noise = self.spectrum_parameters.get('Noise') * max(self.spectrum_data['spectrum'])
        for i in range(len(self.spectrum_data)):
            # data['noisy spectrum'][i] = data['humid spectrum'][i] + random.uniform(0.01, max_noise)
            noisy_spectrum.append(self.spectrum_data.get('spectrum')[i] + random.uniform(0.01, max_noise))
        self.spectrum_data['noisy spectrum'] = noisy_spectrum
        self.spectrum_data['neg noisy spectrum'] = -self.spectrum_data['noisy spectrum']

    def add_water(self): #TODO Implement adding humidity interference in the spectrum
        pass

class Spectrum_parameters:

    def __init__(self, parameters, type):
        self.parameters = parameters
        self.type = type

        self.spect_parameters = {}

        if type == 'batch':
            self.spect_parameters = {
                'STD': random.randint(eval(self.parameters.batch_parameters_lines.get('Std min').text()),
                                      eval(self.parameters.batch_parameters_lines.get('Std max').text())),
                'Noise': round(
                    random.uniform(0.001, eval(self.parameters.batch_parameters_lines.get('Noise level').text())), 4),
                'Drift': random.uniform(-eval(self.parameters.batch_parameters_lines.get('Drift +/- (%)').text()),
                                        eval(self.parameters.batch_parameters_lines.get('Drift +/- (%)').text()))}
        elif type == 'single':
            self.spect_parameters = {
                'STD': eval(self.parameters.single_parameters_lines.get('STD').text()),
                'Noise': eval(self.parameters.single_parameters_lines.get('Noise level').text()),
                'Drift': eval(self.parameters.single_parameters_lines.get('Drift (%)').text())}

class Data_batch:

    def __init__(self, parameters, window, molset):

        self.parameters = parameters
        self.win = window
        self.molset = molset

        self.molecules_batch = {}
        for key in self.molset.molecules_data:
            self.molecules_batch[key] = 0

        self.sp = Spectrum_parameters(self.parameters, 'batch')

        self.X = []
        self.y = []

        self.produce_data_batch()

    def produce_data_batch(self):

        X = []
        y = []

        def process(mol):
            self.win.set_image(mol)
            mol_modes = self.molset.molecules_data.get(mol)
            self.molecules_batch[mol] += 1

            spectrum = Spectrum(mol, mol_modes, self.sp.spect_parameters)
            self.win.plot_main(spectrum)

            X.append(spectrum.spectrum_data['input'])
            y.append(mol)

        if self.win.balance_checkbox.isChecked() == True:
            actual_batch_size = round(eval(self.parameters.batch_parameters_lines.get('Batch size').text()) / len(self.molset.molecules_data.keys()), 0) * len(self.molset.molecules_data.keys())
            batch_size = 0
            mol_count = {}

            for key in self.molset.molecules_data:
                mol_count[key] = round(eval(self.parameters.batch_parameters_lines.get('Batch size').text()) / len(self.molset.molecules_data.keys()), 0)

            while batch_size != actual_batch_size:
                mol = self.molset.molecules_list[random.randint(0, len(self.molset.molecules_list) - 1)]

                if mol_count.get(mol) != 0:
                    process(mol)

                    batch_size = batch_size + 1
                    mol_count[mol] = mol_count[mol] - 1

                    self.win.set_progress((batch_size * 100) / actual_batch_size,
                                          'Generating batch...')
        else:
            for i in range(eval(self.parameters.batch_parameters_lines.get('Batch size').text())):
                mol = self.molset.molecules_list[random.randint(0, len(self.molset.molecules_list) - 1)]

                process(mol)

                self.win.set_progress((i * 100) / eval(self.parameters.batch_parameters_lines.get('Batch size').text()),
                                      'Generating batch...')

        X = np.array(X)
        y = np.array(y)

        self.batch = {'X': X, 'y': y}

